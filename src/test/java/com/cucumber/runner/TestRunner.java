package com.cucumber.runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.cucumber.BaseClass.BaseClass;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions(
		features="src\\test\\java\\com\\cucumber\\Feature",
		glue="com\\cucumber\\stepdefinition",
		plugin= {"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:Report/reports.html"})

public class TestRunner {
	public static WebDriver driver;
	@BeforeClass
	public static void browserlaunch() {
		driver=BaseClass.browseropen("chrome");
	}
	
	@AfterClass
	public static void tearDown() {
		BaseClass.browserclose();
	}
	
	

}
