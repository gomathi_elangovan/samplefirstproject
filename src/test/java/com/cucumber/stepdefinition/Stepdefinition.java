package com.cucumber.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.cucumber.BaseClass.BaseClass;
import com.cucumber.pom.HomePage;
import com.cucumber.pom.LoginPage;
import com.cucumber.runner.TestRunner;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;



public class Stepdefinition extends BaseClass
{
	public static WebDriver driver=TestRunner.driver;
	@Given("user is on facebook application page")
	public void user_is_on_facebook_application_page() {
		BaseClass.urlpass("http://adactin.com/HotelApp/index.php");
	}
	
	@When("User enters valid username")
	public void user_enters_valid_username() {
		WebElement email = driver.findElement(By.id("username"));
		BaseClass.inputtext(email, "gomathielangovan1705");
		
	}
	
	@When("User enters valid password")
	public void user_enters_valid_password() {
		WebElement pswd = driver.findElement(By.id("password"));
		BaseClass.inputtext(pswd, "gomathigomathi");
		
	}

	@When("User click on the sign in button")
	public void user_click_on_the_sign_in_button() {
		WebElement loginbtn = driver.findElement(By.id("login"));
		BaseClass.loginbtn(loginbtn);
	}

	@Then("User verify the username in the home page")
	public void user_verify_the_username_in_the_home_page() {
		WebElement verify = driver.findElement(By.id("username_show"));
		System.out.println(verify.getAttribute("value"));
		String expectedname="Hello Gomathielangovan1705!";
		if(verify.getAttribute("value").contains(expectedname))
				{
			System.out.println("Valid User");
				}
		else {
			System.out.println("invalid User");
		}
		
	}
	@Given("user is on adactin application page")
	public void user_is_on_adactin_application_page() {
		BaseClass.urlpass("http://adactin.com/HotelApp/index.php");
	}

	@When("user enter valid username and password")
	public void user_enter_valid_username_and_password() {
		LoginPage l=new LoginPage(driver);
		driver=BaseClass.loginpageinput(l.getUsername(), "gomathielangovan1705");
		driver=BaseClass.loginpageinput(l.getPswd(), "gomathigomathi");
		driver=BaseClass.loginbtn(l.getLoginbtn());
		}

	@When("select location as syndey")
	public void select_location_as_syndey() {
		HomePage h=new HomePage(driver);
	    driver=BaseClass.homepagedetails(h.getLocation(),"Sydney");
	}

	@When("select hotel as hotel creek")
	public void select_hotel_as_hotel_creek() {
		HomePage h=new HomePage(driver);
		driver=BaseClass.homepagedetails(h.getHotel(), "Hotel Creek");
		
	}

	@When("select roomtype as standard")
	public void select_roomtype_as_standard() {
		HomePage h=new HomePage(driver);
		driver=BaseClass.homepagedetails(h.getRoomtype(), "Standard");
	}

	/*@When("select no of rooms as one")
	public void select_no_of_rooms_as_one() {
		HomePage h=new HomePage(driver);
		driver=BaseClass.homepagedetails(h.getNoofRooms(), "1 - One");
	}*/

	@When("enter checkin date later the checkout date feild")
	public void enter_checkin_date_later_the_checkout_date_feild() {
		HomePage h=new HomePage(driver);
		driver=BaseClass.inputclear(h.getCheckindate());
	    driver=BaseClass.inputclear(h.getCheckoutdate());
	    driver=BaseClass.inputtext(h.getCheckindate(), "14/11/2019");
	    driver=BaseClass.inputtext(h.getCheckoutdate(), "16/11/2019");
	}
	@When("select no of rooms as one")
	public void select_no_of_adults_in_room() {
		HomePage h=new HomePage(driver);
		driver=BaseClass.homepagedetails(h.getNoofRooms(), "1 - One");
		driver=BaseClass.homepagedetails(h.getChildroom(), "1 - One");
	    driver=BaseClass.loginbtn(h.getSubmit());
	}



}
