package com.cucumber.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	public static WebDriver driver;
	@FindBy(id="username")
	private WebElement username;
	
	@FindBy(id="password")
	private WebElement pswd;
	
	@FindBy(id="login")
	private WebElement loginbtn;
	
	public LoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	public WebElement getUsername() {
		return username;
	}
	
	public WebElement getPswd() {
		return pswd;
	}
	public WebElement getLoginbtn() {
		return loginbtn;
	}
}
