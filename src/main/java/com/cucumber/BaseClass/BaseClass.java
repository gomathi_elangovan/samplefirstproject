package com.cucumber.BaseClass;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;

public class BaseClass {
	public static WebDriver driver;
		public static WebDriver browseropen(String browsername) {
			try {
				if(browsername.equalsIgnoreCase("chrome"))
				{
					System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Driver\\chromedriver.exe");
					driver=new ChromeDriver();
				}else if(browsername.equalsIgnoreCase("ie"))
				{
					System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") +"browserDriver\\internetexplorerdriver.exe");
					driver=new InternetExplorerDriver();
				}else if(browsername.equalsIgnoreCase("firefox"))
				{
					System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") +"browserDriver\\Firefoxdriver.exe");
					driver=new FirefoxDriver();
				}else
				{
					System.out.println("Invalid BrowserName");
				}
				driver.manage().window().maximize();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return driver;
		}
		public static void browserclose() {
			try {
				driver.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public static WebDriver urlpass(String url) {
			try {
				driver.get(url);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return driver;
		}
		
		public static WebDriver loginpageinput(WebElement element, String value) {
			try {
				element.sendKeys(value);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return driver;
		}
		
		public static WebDriver loginbtn(WebElement element) {
			try {
				element.click();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return driver;
		}
		
		public static WebDriver homepagedetails(WebElement element, String value)
		{
			try {
				Select s=new Select(element);
				s.selectByVisibleText(value);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return driver;
		}
		//public static WebDriver submitbtn(WebElement element) {
			//element.click(); 
			//return driver;
		//}
		public static WebDriver inputtext(WebElement element, String value) {
			try {
				element.sendKeys(value);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return driver;
		}
		public static WebDriver inputclear(WebElement element) {
			try {
				element.clear();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return driver;
		}
		
}
